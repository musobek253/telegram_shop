package uz.pdp.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import uz.pdp.shop.bot.Main;
import uz.pdp.shop.bot.category.CategoryServiceBot;
import uz.pdp.shop.bot.product.ProductServiceBot;

@SpringBootApplication
public class ShopApplication implements WebMvcConfigurer, CommandLineRunner {

    @Autowired
    private Main main;


    public static void main(String[] args) {
        SpringApplication.run(ShopApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

        try {
            ApiContextInitializer.init();
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            telegramBotsApi.registerBot(new Main().telegramLongPollingBot());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
