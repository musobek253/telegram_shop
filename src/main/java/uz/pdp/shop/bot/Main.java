package uz.pdp.shop.bot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.shop.bot.category.CategoryServiceBot;
import uz.pdp.shop.bot.product.ProductServiceBot;

import java.util.ArrayList;
import java.util.List;

@Component
public class Main {

    @Autowired
    private CategoryServiceBot categoryServiceBot;

    @Autowired
    private ProductServiceBot productServiceBot;

    private Long userChatId;
    private String userMessage;

    @Bean
    public TelegramLongPollingBot telegramLongPollingBot() {
        return new TelegramLongPollingBot(new DefaultBotOptions()) {
            @Override
            public void onUpdateReceived(Update update) {
                if (update.hasMessage()) {
                    userChatId = update.getMessage().getChatId();
                    userMessage = update.getMessage().getText();
                    switch (userMessage) {
                        case "/start":
                            menu();
                            break;
                        case "Buy product":
                            InlineKeyboardMarkup categoryList = categoryServiceBot.getCategoryList(0);
                            userMessage = "Kerakli Kategoryani tanlang";
                            break;
                        case "My Orders":
                            break;
                        case "Cart":
                            break;
                    }
                }
            }

            @Override
            public String getBotUsername() {
                return "t.me/megabonuzbot";
            }

            @Override
            public String getBotToken() {
                return "1761385621:AAEN9HUtpBW0KmZZrYVbm9lZVLPYWio-iMs";
            }


            private void execute(
                    ReplyKeyboardMarkup replyKeyboardMarkup,
                    InlineKeyboardMarkup inlineKeyboardMarkup
            ) {
                SendMessage sendMessage = new SendMessage();
                sendMessage.setChatId(userChatId);
                sendMessage.setText(userMessage);

                if (replyKeyboardMarkup != null) {
                    sendMessage.setReplyMarkup(replyKeyboardMarkup);
                    replyKeyboardMarkup.setResizeKeyboard(true);
                    replyKeyboardMarkup.setOneTimeKeyboard(true);
                    replyKeyboardMarkup.setSelective(true);
                }
                if (inlineKeyboardMarkup != null)
                    sendMessage.setReplyMarkup(inlineKeyboardMarkup);

                try {
                    execute(sendMessage);
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }

            public ReplyKeyboardMarkup menu() {
                ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
                List<KeyboardRow> keyboardRows = new ArrayList<>();
                KeyboardRow keyboardRow = new KeyboardRow();
                KeyboardRow keyboardRow1 = new KeyboardRow();
                keyboardRow.add("Buy product");
                keyboardRow1.add("My Orders");
                keyboardRow1.add("Cart");
                keyboardRows.add(keyboardRow);
                keyboardRows.add(keyboardRow1);
                replyKeyboardMarkup.setKeyboard(keyboardRows);
                execute(replyKeyboardMarkup, null);

                return replyKeyboardMarkup;
            }
        };
    }





}
