package uz.pdp.shop.bot.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import uz.pdp.shop.entity.category.CategoryDatabase;
import uz.pdp.shop.entity.product.ProductDatabase;
import uz.pdp.shop.repository.ProductRepository;
import uz.pdp.shop.service.product.ProductService;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceBot {

    private ProductService productService;
    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceBot(ProductService productService, ProductRepository productRepository) {
        this.productService = productService;
        this.productRepository = productRepository;
    }

    public InlineKeyboardMarkup getProductList(
            CategoryDatabase categoryDatabase
    ) {
        List<ProductDatabase> productDatabaseList = productRepository.findAllByCategoryDatabase(categoryDatabase);

        List<List<InlineKeyboardButton>> mainList = new ArrayList<>();
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(mainList);

        List<InlineKeyboardButton> keyButtonRow = new ArrayList<>();
        int counter = 0;

        for (ProductDatabase productDatabase : productDatabaseList) {
            keyButtonRow.add(new InlineKeyboardButton(productDatabase.getName())
                    .setCallbackData(
                            String.valueOf(
                                    productDatabase.getId())
                    ));

            counter++;

            if (counter % 3 == 0) {
                mainList.add(keyButtonRow);
                keyButtonRow = new ArrayList<>();
            }

        }
        if (!keyButtonRow.isEmpty())
            mainList.add(keyButtonRow);
        return inlineKeyboardMarkup;
    }


}

